import os
import requests
from django.utils import timezone
from dotenv import load_dotenv

from .models import Message


def save_results(mailing, sending_status=None):
    return Message.objects.create(
        date_time_send=timezone.now(),
        sending_status=sending_status,
        mailing_id=mailing
    )


def send_message(phone_number, text, mailing):
    load_dotenv()
    headers = {
    'Authorization': f'Bearer {os.getenv("TOKEN")}'
    }
    result_massage = save_results(mailing=mailing)
    data = {
        'id': result_massage.id,
        'phone': phone_number,
        'text': text
    }
    url = f'https://probe.fbrq.cloud/v1/send/{result_massage.id}'
    try:
        response = requests.post(url=url, headers=headers, json=data)
        result_massage.sending_status = response.status_code
    except requests.exceptions.ConnectionError:
        result_massage.sending_status = 'Connection error'
    except requests.exceptions.Timeout:
        result_massage.sending_status = 'Timeout'
    result_massage.save()
