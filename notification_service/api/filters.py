from django_filters import filters
from django_filters.rest_framework import FilterSet

from .models import Message


class SendingStatusFilter(FilterSet):
    sending_status = filters.ModelMultipleChoiceFilter(
        queryset=Message.objects.all(),
        to_field_name='sending_status'
    )

    class Meta:
        model = Message
        fields = ('sending_status',)
