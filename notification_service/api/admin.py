from django.contrib import admin

from .models import Client, FilterTag, Mailing, Message


class MessageAdmin(admin.ModelAdmin):
    list_display = ('date_time_send', 'sending_status', 'mailing_id')


admin.site.register(Mailing)
admin.site.register(Client)
admin.site.register(Message, MessageAdmin)