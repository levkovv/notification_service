from django.utils import timezone
from rest_framework import serializers

from .models import Client, FilterTag, Mailing, Message
from .tasks import send_mailing


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = '__all__'


class FilterTagSerializer(serializers.ModelSerializer):

    class Meta:
        model = FilterTag
        fields = '__all__'


class MailingSerializer(serializers.ModelSerializer):
    filters = FilterTagSerializer(many=True)

    class Meta:
        model = Mailing
        fields = '__all__'

    def validate(self, attrs):
        if attrs['date_time_start'] > attrs['date_time_end']:
            raise serializers.ValidationError(
                'Окончание рассылки не может быть раньше начала')
        return super().validate(attrs)

    def save_filters(self, mailing, filters):
        filters_to_save = []
        for filter in filters:
            current_filter, status = FilterTag.objects.get_or_create(**filter)
            filters_to_save.append(current_filter)
        mailing.filters.set(filters_to_save)

    def create(self, validated_data):
        filters = validated_data.pop('filters')
        mailing = Mailing.objects.create(**validated_data)
        self.save_filters(mailing, filters)
        if timezone.now() > mailing.date_time_start:
            send_mailing.delay(mailing.id)
        if timezone.now() < mailing.date_time_start:
            send_mailing.apply_async(
                (mailing.id,),
                countdown=(
                    mailing.date_time_start - timezone.now()).total_seconds())
        return mailing

    def update(self, instance, validated_data):
        filters = validated_data.pop('filters')
        self.save_filters(instance, filters)
        return super().update(instance, validated_data)


class StatisticSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = '__all__'
