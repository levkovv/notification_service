from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.utils import timezone

from api.models import Client, Mailing
from notification_service.celery import app
from .service import send_message


@app.task
def send_mailing(mailing_id):
    mailing = get_object_or_404(Mailing, id=mailing_id)
    filters = mailing.filters.all()
    clients = set()
    for filter in filters:
        client_queryset = set(
            Client.objects.filter(
                Q(mobile_operator_code=filter.value) |
                Q(tag=filter.value)))
        clients.update(client_queryset)
    for client in clients:
        if timezone.now() < mailing.date_time_end:
            send_message(client.phone_number, mailing.text, mailing)

