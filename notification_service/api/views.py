from django.shortcuts import get_list_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from .filters import SendingStatusFilter
from .models import Client, Mailing, Message
from .serializers import (ClientSerializer, MailingSerializer,
                          StatisticSerializer)


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    http_method_names = ('get', 'post', 'patch', 'delete')


class MailingViewSet(viewsets.ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()
    http_method_names = ('get', 'post', 'patch', 'delete')


class StatisticViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = StatisticSerializer
    queryset = Message.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filterset_class = SendingStatusFilter


class MailingStatisticViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = StatisticSerializer

    def get_queryset(self):
        return get_list_or_404(Message, mailing_id=self.kwargs['mailing_id'])
