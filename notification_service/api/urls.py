from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import (ClientViewSet, MailingStatisticViewSet, MailingViewSet,
                    StatisticViewSet)

router = DefaultRouter()
router.register(r'clients', ClientViewSet)
router.register(r'mailings', MailingViewSet)
router.register(r'statistic', StatisticViewSet)
router.register(
    r'mailing_statistic/(?P<mailing_id>\d+)',
    MailingStatisticViewSet, basename='mailing_statistic')


urlpatterns = [
    path('', include(router.urls)),
]
