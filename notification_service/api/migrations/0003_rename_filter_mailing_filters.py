# Generated by Django 3.2.12 on 2022-02-07 13:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20220207_1305'),
    ]

    operations = [
        migrations.RenameField(
            model_name='mailing',
            old_name='filter',
            new_name='filters',
        ),
    ]
