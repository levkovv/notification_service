import pytz
from django.core.validators import RegexValidator
from django.db import models

TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))


class FilterTag(models.Model):
    value = models.SlugField('Тэг или код оператора')

    def __str__(self) -> str:
        return self.value


class Mailing(models.Model):
    date_time_start = models.DateTimeField(
        'Дата и время начала рассылки')
    text = models.TextField(
        'Текст сообщения')
    filters = models.ManyToManyField(
        FilterTag,
        verbose_name='Фильтры для отправки сообщений')
    date_time_end = models.DateTimeField(
        'Дата и время окончания рассылки')

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self) -> str:
        return str(self.id)


class Client(models.Model):
    phone_number = models.CharField(
        'Номер телефона', max_length=11,
        unique=True,
        validators=[RegexValidator(
            regex=r'7\d{10}',
            message='Не правильный формат телефонного номера')])
    mobile_operator_code = models.CharField(
        'Код мобильного оператора', max_length=3,
        validators=[RegexValidator(
            regex=r'9\d{2}',
            message='Не правильный код оператора')])
    tag = models.SlugField('Тэг')
    time_zone = models.CharField(
        'Часовой пояс', max_length=32,
        choices=TIMEZONES)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self) -> str:
        return self.phone_number


class Message(models.Model):
    date_time_send = models.DateTimeField(
        'Дата и время отправки сообщения')
    sending_status = models.CharField(
        'Статус отправки сообщения', max_length=128,
        blank=True, null=True)
    mailing_id = models.ForeignKey(
        Mailing, verbose_name='ID рассылки',
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ('sending_status',)
