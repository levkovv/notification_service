from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

schema_view = get_schema_view(
   openapi.Info(
      title="Notification Service",
      default_version='v1',
      description="Документация для тестового проекта 'Сервис уведомлений'",
      contact=openapi.Contact(email="levkovv@gmail.com"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
   path('admin/', admin.site.urls),
   path('api/v1/', include('api.urls')),
   url(
      r'^docs(?P<format>\.json|\.yaml)$',
      schema_view.without_ui(cache_timeout=0), name='schema-json'),
   url(
      r'^docs/$', schema_view.with_ui('swagger', cache_timeout=0), 
      name='docs'),
]
